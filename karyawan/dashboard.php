<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <section class="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">CV Restu Jaya</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Absensi</a>
                    </li>
                </ul>
                <?php
                session_start();
                ?>
                <form class="form-inline my-2 my-lg-0">
                    <a href="../logout.php" class="btn btn-primary" onclick="return confirm('Yakin ingin Logout?')">Log
                        out</a>
                </form>
            </div>
        </nav>
    </section>

    <?php
    include '../koneksi.php';
    $id_users = $_SESSION['id_users'];
    $tanggal = date("Y-m-d");
    $query = "SELECT * FROM kehadiran WHERE kehadiran_user_id={$id_users} AND DATE(tanggal)={$tanggal}";
    $data = mysqli_query($koneksi, $query);
    if ($data) {
        $data = mysqli_fetch_object($data);
    }
    if (isset($_POST['absen'])) {
        $status = $_POST['status'];
        $query = "INSERT INTO kehadiran (kehadiran_user_id,status) VALUES('$id_users','$status')";
        if ($data){
            $query = "UPDATE kehadiran SET status='$status' WHERE kehadiran_user_id='$id_users' AND DATE(tanggal)=$tanggal";
        }
        if (mysqli_query($koneksi, $query)) {
            echo "<script LANGUAGE='JavaScript'>
          window.alert('Proses Absensi Berhasil')
          window.location.href='dashboard.php';
          </script>";
        } else {
            echo "Error: " . $daftar . "<br>" . mysqli_error($koneksi);
        }
    }

    ?>

    <section class="dashboard mt-2">
        <span class="badge badge-primary"><?php echo $_SESSION['username']; ?></span>
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Absensi Karyawan</li>
                    </ol>
                </nav>
                <form class="row justify-content-center" action="" method="post">
                    <div class="col-md-4">
                        <div class="form-group">
                            <select name="status" class="form-control" required>
                                <option value="">Pilih Status</option>
                                <option value="hadir">Hadir</option>
                                <option value="izin">Izin</option>
                                <option value="sakit">Sakit</option>
                                <option value="alpa">Alpa</option>
                            </select>
                        </div>
                        <div class="form-group text-center" >
                            <input type="submit" class="btn btn-success" name="absen" value="Absen">
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </section>
</div>

<script src="../static/js/jquery.min.js"></script>
<script src="../static/js/jquery.min.js"></script>
</body>
</html>