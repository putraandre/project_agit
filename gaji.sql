-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 06, 2020 at 01:18 PM
-- Server version: 10.4.8-MariaDB-log
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penggajian`
--

-- --------------------------------------------------------

--
-- Table structure for table `gaji`
--

CREATE TABLE `gaji` (
                        `gaji_user_id` int(11) NOT NULL,
                        `pajak` decimal(10,0) DEFAULT NULL,
                        `gaji` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gaji`
--

INSERT INTO `gaji` (`gaji_user_id`, `pajak`, `gaji`) VALUES
(1, '5', '134204'),
(16, '1', '1313');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
                           `jabatan_id` int(11) NOT NULL,
                           `jabatan_nama` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`jabatan_id`, `jabatan_nama`) VALUES
(2, 'jabatan 1'),
(3, 'jabatan 2');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
                            `id_karyawan` int(11) NOT NULL,
                            `nik` varchar(100) DEFAULT NULL,
                            `karyawan_jabatan_id` int(11) NOT NULL,
                            `nama` varchar(100) NOT NULL,
                            `alamat` text NOT NULL,
                            `nohp` char(15) NOT NULL,
                            `status` enum('kontrak','magang','tetap') NOT NULL,
                            `no_rek` varchar(100) DEFAULT NULL,
                            `gaji_pokok` decimal(10,0) DEFAULT NULL,
                            `uang_makan` decimal(10,0) DEFAULT NULL,
                            `tunjangan` decimal(10,0) DEFAULT NULL,
                            `transport` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `nik`, `karyawan_jabatan_id`, `nama`, `alamat`, `nohp`, `status`, `no_rek`, `gaji_pokok`, `uang_makan`, `tunjangan`, `transport`) VALUES
(1, '112233', 2, 'andre', 'dasan baru lombok tengah NTB', '087718879608', 'tetap', '1123', '123123', '4444', '123', '123'),
(14, '223344', 2, 'asdasd', '213123', '123123', 'magang', '1231233', '12123', '123123', '123123', '123123'),
(16, '445566', 2, 'asdf', '123', '123', 'magang', '123', '1000', '100', '100', '100');

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE `kehadiran` (
                             `id_kehadiran` int(6) UNSIGNED NOT NULL,
                             `kehadiran_user_id` int(11) NOT NULL,
                             `status` enum('hadir','izin','sakit','alpa') DEFAULT NULL,
                             `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kehadiran`
--

INSERT INTO `kehadiran` (`id_kehadiran`, `kehadiran_user_id`, `status`, `tanggal`) VALUES
(1, 1, 'hadir', '2020-07-25 14:17:34'),
(2, 1, 'hadir', '2020-08-05 17:48:51'),
(3, 1, 'hadir', '2020-07-26 18:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
                         `id_users` int(11) NOT NULL,
                         `username` varchar(100) NOT NULL,
                         `password` char(32) NOT NULL,
                         `level` enum('1','2','3') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `level`) VALUES
(1, 'admin', '5f4dcc3b5aa765d61d8327deb882cf99', '1'),
(14, 'g9insane', '912ec803b2ce49e4a541068d495ab570', '1'),
(16, 'test', 'd41d8cd98f00b204e9800998ecf8427e', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gaji`
--
ALTER TABLE `gaji`
    ADD PRIMARY KEY (`gaji_user_id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
    ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
    ADD UNIQUE KEY `karyawan_id_karyawan_uindex` (`id_karyawan`),
    ADD KEY `karyawan_jabatan_jabatan_id_fk` (`karyawan_jabatan_id`);

--
-- Indexes for table `kehadiran`
--
ALTER TABLE `kehadiran`
    ADD PRIMARY KEY (`id_kehadiran`),
    ADD KEY `kehadiran_users_id_users_fk` (`kehadiran_user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
    MODIFY `jabatan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kehadiran`
--
ALTER TABLE `kehadiran`
    MODIFY `id_kehadiran` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `gaji`
--
ALTER TABLE `gaji`
    ADD CONSTRAINT `gaji_users_id_users_fk` FOREIGN KEY (`gaji_user_id`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `karyawan`
--
ALTER TABLE `karyawan`
    ADD CONSTRAINT `karyawan_jabatan_jabatan_id_fk` FOREIGN KEY (`karyawan_jabatan_id`) REFERENCES `jabatan` (`jabatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `karyawan_users_id_users_fk` FOREIGN KEY (`id_karyawan`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kehadiran`
--
ALTER TABLE `kehadiran`
    ADD CONSTRAINT `kehadiran_users_id_users_fk` FOREIGN KEY (`kehadiran_user_id`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
