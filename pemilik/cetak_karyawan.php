<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Karyawan</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/DataTables/datatables.min.css"/>
</head>
<body>
<div class="container">
    <h1>Daftar Karyawan</h1>
    <table class="table table-bordered">
      <tr>
        <th>No</th>
        <th>Nik</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>No Handphone</th>
        <th>Status</th>
      </tr>
      <?php
      include "../koneksi.php";
			$no = 1;
			$sql = mysqli_query($koneksi,"select * from karyawan");
			while($data = mysqli_fetch_array($sql)){
			?>
			<tr>
				<td><?php echo $no++; ?></td>
				<td><?php echo $data['nik']; ?></td>
				<td><?php echo $data['nama']; ?></td>
				<td><?php echo $data['alamat']; ?></td>
        <td><?php echo $data['nohp']; ?></td>
				<td><?php echo $data['status']; ?></td>
			</tr>
			<?php 
			}
			?>
    </table>
    <script>
      window.print();
    </script>
</div>

<script src="../static/js/jquery.min.js"></script>
<script src="../static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../static/DataTables/datatables.min.js"></script>
</body>
</html>