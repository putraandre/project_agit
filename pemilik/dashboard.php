<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/DataTables/datatables.min.css"/>
</head>
<body>
<div class="container">
    <section class="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">CV Restu Jaya</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Karyawan</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="absensi.php">Absen</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="gaji.php">Gaji</a>
                    </li>
                </ul>
                <?php
                session_start();
                ?>
                <form class="form-inline my-2 my-lg-0">
                    <a href="../logout.php" class="btn btn-primary" onclick="return confirm('Yakin ingin Logout?')">Log
                        out</a>
                </form>
            </div>
        </nav>
    </section>

    <section class="dashboard mt-2">
        <span class="badge badge-primary"><?php echo $_SESSION['username']; ?></span>
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-between">
                        <li class="breadcrumb-item active" aria-current="page">Daftar Karyawan</li>
                        <li><a href="tambah_karyawan.php" class="btn btn-sm btn-outline-primary">+ Tambah Data Karyawan</a></li>
                    </ol>

                </nav>

                <table class="table table-sm table-bordered mt-2">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nik</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">No Handphone</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    include "../koneksi.php";

                    $no = 1;
                    $data = mysqli_query($koneksi, "SELECT * FROM users AS u LEFT JOIN karyawan k on u.id_users = k.id_karyawan");
                    while ($row = mysqli_fetch_array($data)) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $row['nik']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['alamat']; ?></td>
                            <td><?php echo $row['nohp']; ?></td>
                            <td>
                                <span class="badge badge-primary"><?php echo $row['status']; ?></span>
                            </td>
                            <td>
                                <a href="update_karyawan.php?id=<?php echo $row['id_users']; ?>"
                                   class="btn btn-sm btn-outline-success">Update</a> |
                                <a href="delete_karyawan.php?id_karyawan=<?php echo $row['id_users']; ?>"
                                   class="btn btn-sm btn-outline-danger"
                                   onclick="return confirm('Yakin ingin Menghapus Data Ini?')">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../static/DataTables/datatables.min.js"></script>
<script type="text/javascript">
    $(() => {
        $('.table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        })
    })
</script>
</body>
</html>