<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Gaji <?= $_GET['m'] ? date('F', mktime(0, 0, 0, $_GET['m'], 10)) : ''?></title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/DataTables/datatables.min.css"/>
</head>
<body>

<div class="container">
    <section class="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">CV Restu Jaya</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Karyawan</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="laporan_absensi.php">Absen</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="laporan_gaji.php">Gaji</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="../logout.php" class="btn btn-primary" onclick="return confirm('Yakin ingin Logout?')">Log
                        out</a>
                </form>
            </div>
        </nav>
    </section>
    <section class="dashboard">
        <div class="row">
            <div class="col-md-12 mt-2">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Daftar Gaji Karyawan</li>
                    </ol>
                </nav>
                <div class="col-4">
                    <div class="form-group">
                        <select class="form-control" name="bulan" id="bulan" onchange="if (this.value) window.location.href='?m='+this.value">
                            <option value="">--Pilih Bulan--</option>
                            <option value="1" <?= @$_GET['m']==1 ? 'selected' : null ?>>Januari</option>
                            <option value="2" <?= @$_GET['m']==2 ? 'selected' : null ?>>Februari</option>
                            <option value="3" <?= @$_GET['m']==3 ? 'selected' : null ?>>Maret</option>
                            <option value="4" <?= @$_GET['m']==4 ? 'selected' : null ?>>April</option>
                            <option value="5" <?= @$_GET['m']==5 ? 'selected' : null ?>>Mei</option>
                            <option value="6" <?= @$_GET['m']==6 ? 'selected' : null ?>>Juni</option>
                            <option value="7" <?= @$_GET['m']==7 ? 'selected' : null ?>>Juli</option>
                            <option value="8" <?= @$_GET['m']==8 ? 'selected' : null ?>>Agustus</option>
                            <option value="9" <?= @$_GET['m']==9 ? 'selected' : null ?>>September</option>
                            <option value="10" <?= @$_GET['m']==10 ? 'selected' : null ?>>Oktober</option>
                            <option value="11" <?= @$_GET['m']==11 ? 'selected' : null ?>>November</option>
                            <option value="12" <?= @$_GET['m']==12 ? 'selected' : null ?>>Desember</option>
                        </select>
                    </div>
                </div>
                <table class="table table-sm table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nik</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Hadir</th>
                            <th scope="col">Izin</th>
                            <th scope="col">Sakit</th>
                            <th scope="col">Alpa</th>
                            <th scope="col">Rekening</th>
                            <th scope="col">Gaji Pokok</th>
                            <th scope="col">Tunjangan</th>
                            <th scope="col">Transport</th>
                            <th scope="col">Total</th>
                            <th scope="col">Pajak</th>
                            <th scope="col">Total Bersih</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                    include "../koneksi.php";
                    if (isset($_GET['m'])) {
                        $where = "WHERE month(tanggal)={$_GET['m']}";
                        $no = 1;
                        $data = mysqli_query($koneksi, "SELECT *,
                                sum(CASE WHEN k2.status = 'hadir' then 1 else 0 end )as hadir,
                                sum(CASE WHEN k2.status = 'izin' then 1 else 0 end) as izin,
                                sum(CASE WHEN k2.status = 'sakit' then 1 else 0 end )as sakit,
                                sum(CASE WHEN k2.status = 'alpa' then 1 else 0 end )as alpa
                            FROM users as u
                         JOIN karyawan k on k.id_karyawan = u.id_users
                         JOIN kehadiran k2 on u.id_users = k2.kehadiran_user_id
                         LEFT JOIN gaji g on u.id_users = g.gaji_user_id
                        {$where}
                         GROUP BY u.id_users
             ");

                        while ($row = mysqli_fetch_array($data)) {
                            ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $row['nik'] ?></td>
                                <td><?= $row['nama'] ?></td>
                                <td><?= $row['hadir'] ?></td>
                                <td><?= $row['izin'] ?></td>
                                <td><?= $row['sakit'] ?></td>
                                <td><?= $row['alpa'] ?></td>
                                <td><?= $row['no_rek'] ?></td>
                                <td><?= $row['gaji_pokok'] ?></td>
                                <td><?= $row['tunjangan'] ?></td>
                                <td><?= $row['transport'] ?></td>
                                <td><?= $row['gaji_pokok'] + $row['tunjangan'] + $row['transport']?></td>
                                <td><?= ($row['gaji_pokok'] + $row['tunjangan'] + $row['transport'])*$row['pajak']?></td>
                                <td><?= $row['gaji'] ?></td>
                            </tr>
                            <?php
                        }}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<script src="../static/js/jquery.min.js"></script>
<script src="../static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../static/DataTables/datatables.min.js"></script>
<script type="text/javascript">
    $(() => {
        $('.table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        })
    })
</script>
</body>
</html>