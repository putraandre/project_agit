<?php
	$db_host = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "penggajian";

	$koneksi = mysqli_connect($db_host, $db_user, $db_pass, $db_name);
	if(mysqli_connect_errno()){
		echo 'Gagal melakukan koneksi ke Database : '.mysqli_connect_error();
	}



    function menu() {
        session_start();
        return '<section class="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">CV Restu Jaya</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Karyawan</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="jabatan.php">Jabatan</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="absensi.php">Absen</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="gaji.php">Gaji</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="../logout.php" class="btn btn-primary" onclick="return confirm(\'Yakin ingin Logout?\')">Log
                        out</a>
                </form>
            </div>
        </nav>
    </section>';
    }


