<?php
include "../koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/DataTables/datatables.min.css"/>
</head>
<body>
<div class="container">
    <?= menu()?>

    <section class="dashboard mt-2">
        <span class="badge badge-primary"><?php echo $_SESSION['username']; ?></span>
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-between">
                        <li class="breadcrumb-item active" aria-current="page">Daftar Karyawan</li>
                        <li><a href="tambah_karyawan.php" class="btn btn-sm btn-outline-primary">+ Tambah Data Karyawan</a></li>
                    </ol>

                </nav>

                <table class="table table-sm table-bordered mt-2">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nik</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Jabatan</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">No Handphone</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php



                    $no = 1;
                    $data = mysqli_query($koneksi, "SELECT * FROM users AS u LEFT JOIN karyawan k on u.id_users = k.id_karyawan LEFT JOIN jabatan ON k.karyawan_jabatan_id=jabatan_id");
                    while ($row = mysqli_fetch_array($data)) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $row['nik']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td><?php echo $row['jabatan_nama']; ?></td>
                            <td><?php echo $row['alamat']; ?></td>
                            <td><?php echo $row['nohp']; ?></td>
                            <td>
                                <span class="badge badge-primary"><?php echo $row['status']; ?></span>
                            </td>
                            <td>
                                <a href="update_karyawan.php?id=<?php echo $row['id_users']; ?>"
                                   class="btn btn-sm btn-outline-success">Update</a> |
                                <a href="delete_karyawan.php?id_karyawan=<?php echo $row['id_users']; ?>"
                                   class="btn btn-sm btn-outline-danger"
                                   onclick="return confirm('Yakin ingin Menghapus Data Ini?')">Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../static/DataTables/datatables.min.js"></script>
<script type="text/javascript">
    $(() => {
        $('.table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        })
    })
</script>
</body>
</html>