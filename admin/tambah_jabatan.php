<?php

include "../koneksi.php";
if (isset($_POST['submit'])) {
    $prefix = "VALUES (NULL,'{$_POST['jabatan_nama']}')";
    if (isset($_GET['id'])){
        $prefix = "VALUES ({$_GET['id']},'{$_POST['jabatan_nama']}')";
    }
    $users = "INSERT INTO jabatan (jabatan_id,jabatan_nama)
                {$prefix}
                    ON DUPLICATE KEY UPDATE
                jabatan_nama = '{$_POST['jabatan_nama']}'";
    if (mysqli_query($koneksi, $users)) {

            echo "<script LANGUAGE='JavaScript'>
                window.alert('Data Jabatan Berhasil Tersimpan')
                window.location.href='jabatan.php';
            </script>";
    } else {
        echo "Error: " . $users . "<br>" . mysqli_error($koneksi);
    }
}


if (isset($_GET['id'])) {
    $query = "SELECT * FROM jabatan WHERE jabatan_id = {$_GET['id']}";
    $data =mysqli_query($koneksi,$query);
    if ($data){
        $data = mysqli_fetch_object($data);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= @$data->jabatan_id ? 'Update' : 'Tambah'?> Jabatan</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <?= menu() ?>

    <section class="dashboard mt-2">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page"><?= @$data->jabatan_id ? 'Update' : 'Tambah'?> Data Jabatan</li>
                    </ol>
                </nav>
                <a href="jabatan.php" class="btn btn-sm btn-outline-primary">Kembali</a><br><br>
                <form action="" method="POST">
                    <input type="hidden" name="id_jabatan" value="<?= @$data->id_jabatan ?: null ?>" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="jabatan_nama">Nama Jabatan</label>
                                <input type="text" class="form-control" id="jabatan_nama" name="jabatan_nama"
                                       placeholder="masukkan nama jabatan" required value="<?= @$data->jabatan_nama ?: null ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary" name="submit">Simpan</button>
                </form>
            </div>
        </div>
    </section>
</div>

<script src="../static/js/bootstrap.min.js"></script>
<script src="../static/js/jquery.min.js"></script>
</body>
</html>