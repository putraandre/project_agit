<?php

include "../koneksi.php";
if (isset($_POST['submit'])) {
    $id_kehadiran = $_POST['$id_kehadiran'];
    $karyawan_id = $_POST['$karyawan_id'];
    $status = $_POST['$status'];

    $q2= "INSERT INTO kehadiran (karyawan_id, status) 
    VALUES ({$karyawan_id}, {$status})";

    if (mysqli_query($koneksi,$q2)){
        echo "<script LANGUAGE='JavaScript'>
        window.alert('Data Absensi Berhasil Ditambahkan')
        window.location.href='dashboard.php';
    </script>";
    }else {
        echo "Error: " .$q2 . "<br>" . mysqli_error($koneksi);
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tambah Absensi</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <section class="menu">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">CV Restu Jaya</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard.php">Karyawan</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="absensi.php">Absen</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="gaji.php">Gaji</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <a href="../logout.php" class="btn btn-primary" onclick="return confirm('Yakin ingin Logout?')">Log out</a>
                </form>
            </div>
        </nav>
    </section>

    <section class="dashboard mt-2">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Tambah Daftar Karyawan</li>
                    </ol>
                </nav>
                <a href="absensi.php" class="btn btn-sm btn-outline-primary">< Kembali</a><br><br>
                <form action="" method="POST">
                    <div class="row">

                    </div>
                    <button type="submit" class="btn btn-outline-primary" name="submit">Simpan</button>
                </form>
            </div>
        </div>
    </section>
</div>

<script src="../static/js/bootstrap.min.js"></script>
<script src="../static/js/jquery.min.js"></script>
</body>
</html>