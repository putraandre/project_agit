<?php  include("../koneksi.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Presensi</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/DataTables/datatables.min.css"/>
</head>
<body>
<div class="container">
<?= menu()?>
    <?php


    if (isset($_POST['absen'])) {

        $nik = $_POST['nik'];
        $hari = $_POST['hari'];
        $jam = $_POST['jam'];
        $status = $_POST['status'];

        $query1 = mysqli_query($koneksi, "SELECT * FROM karyawan WHERE id_karyawan='$id_karyawan'");

        $query = "INSERT INTO absensi VALUES ('$nik', '$hari', '$jam', '$status')";

        if (mysqli_query($koneksi, $query)) {
            echo "<script LANGUAGE='JavaScript'>
          window.alert('Proses Absensi Berhasil')
          window.location.href='dashboard.php';
          </script>";
        } else {
            echo "Error: " . $daftar . "<br>" . mysqli_error($koneksi);
        }
    }

    ?>

    <section class="dashboard mt-2">
        <span class="badge badge-primary"><?php echo $_SESSION['username']; ?></span>
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Daftar Absensi Karyawan</li>
                    </ol>
                </nav>
                <div class="col-4">
                    <div class="form-group">
                        <select class="form-control" name="bulan" id="bulan" onchange="if (this.value) window.location.href='?m='+this.value">
                            <option value="">--Pilih Bulan--</option>
                            <option value="1" <?= @$_GET['m']==1 ? 'selected' : null ?>>Januari</option>
                            <option value="2" <?= @$_GET['m']==2 ? 'selected' : null ?>>Februari</option>
                            <option value="3" <?= @$_GET['m']==3 ? 'selected' : null ?>>Maret</option>
                            <option value="4" <?= @$_GET['m']==4 ? 'selected' : null ?>>April</option>
                            <option value="5" <?= @$_GET['m']==5 ? 'selected' : null ?>>Mei</option>
                            <option value="6" <?= @$_GET['m']==6 ? 'selected' : null ?>>Juni</option>
                            <option value="7" <?= @$_GET['m']==7 ? 'selected' : null ?>>Juli</option>
                            <option value="8" <?= @$_GET['m']==8 ? 'selected' : null ?>>Agustus</option>
                            <option value="9" <?= @$_GET['m']==9 ? 'selected' : null ?>>September</option>
                            <option value="10" <?= @$_GET['m']==10 ? 'selected' : null ?>>Oktober</option>
                            <option value="11" <?= @$_GET['m']==11 ? 'selected' : null ?>>November</option>
                            <option value="12" <?= @$_GET['m']==12 ? 'selected' : null ?>>Desember</option>
                        </select>
                    </div>
                </div>
                <table class="table table-sm table-bordered mt-2">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nik</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Hadir</th>
                        <th scope="col">Izin</th>
                        <th scope="col">Sakit</th>
                        <th scope="col">Alpa</th>
                        <th scope="col">Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (isset($_GET['m'])) {
                    $where = "WHERE month(tanggal)={$_GET['m']}";
                    $no = 1;
                    $data = mysqli_query($koneksi, "SELECT *,
                                sum(CASE WHEN k2.status = 'hadir' then 1 else 0 end )as hadir,
                                sum(CASE WHEN k2.status = 'izin' then 1 else 0 end) as izin,
                                sum(CASE WHEN k2.status = 'sakit' then 1 else 0 end )as sakit,
                                sum(CASE WHEN k2.status = 'alpa' then 1 else 0 end )as alpa
                            FROM users as u
                         JOIN karyawan k on k.id_karyawan = u.id_users
                         JOIN kehadiran k2 on u.id_users = k2.kehadiran_user_id
                        {$where}
                         GROUP BY u.id_users
             ");
                
                    while ($row = mysqli_fetch_array($data)) {
                        ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $row['nik'] ?></td>
                            <td><?= $row['nama'] ?></td>
                            <td><?= $row['hadir'] ?></td>
                            <td><?= $row['izin'] ?></td>
                            <td><?= $row['sakit'] ?></td>
                            <td><?= $row['alpa'] ?></td>
<!--                            <td>--><?//= date("d-m-Y", strtotime($row['tanggal'])); ?><!--</td>-->
                            <td>
                                <a href=""  class="btn btn-info">
                                    Detail
                                </a>
                                <a href=""  class="btn btn-danger">
                                    Hapus
                                </a>
                            </td>
                        </tr>
                        <?php
                    }}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../static/DataTables/datatables.min.js"></script>
<script type="text/javascript">
    $(()=>{
        $('.table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        })
    })
</script>
</body>
</html>