<?php

include "../koneksi.php";
if (isset($_POST['submit'])) {
    //table users
    $id_users = $_POST['id_users'];
    $username = $_POST['username'];
    $password = md5($_POST['password']);
    $level = $_POST['level'];
    //table karyawan

    $nik = $_POST['nik'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $nohp = $_POST['nohp'];
    $status = $_POST['status'];
    $no_rek = $_POST['no_rek'];
    $gaji_pokok = $_POST['gaji_pokok'];
    $tunjangan = $_POST['tunjangan'];
    $transport = $_POST['transport'];
    $uang_makan = $_POST['uang_makan'];
    $jabatan_id = $_POST['karyawan_jabatan_id'];
    $pajak = $_POST['pajak'];
    $prefix = "VALUES (NULL,'{$username}','{$password}','{$level}')";
    $ii = '';
    $oku = '';
    if (isset($_GET['id'])){
        $prefix = "VALUES ({$id_users},'{$username}','{$password}','{$level}')";
    }
    if (!isset($_GET['id'])||isset($password)) {
        $ii = ',password';
        $oku = "password = '{$password}',";
    }
    $users = "INSERT INTO users (id_users,username{$ii},level)
                {$prefix}
                    ON DUPLICATE KEY UPDATE
                username = '{$username}',
                {$oku}
                level = '{$level}' ";
    if (mysqli_query($koneksi, $users)) {
        if (!isset($id_karyawan)){
            $id_karyawan = "SELECT id_users FROM users WHERE username = '$username' LIMIT 1";
            $result = mysqli_query($koneksi,$id_karyawan);
            $row = mysqli_fetch_array($result, MYSQLI_NUM);
            $id_karyawan = (int)$row[0];
        }
        $karyawan = "INSERT INTO karyawan (id_karyawan,nik,karyawan_jabatan_id,nama,alamat,nohp,status,no_rek,gaji_pokok,tunjangan,transport,uang_makan)
                VALUES ({$id_karyawan},'{$nik}','{$jabatan_id}','{$nama}','{$alamat}','{$nohp}','{$status}','{$no_rek}','{$gaji_pokok}','{$tunjangan}','{$transport}','{$uang_makan}')
                    ON DUPLICATE KEY UPDATE
                nik = '{$nik}',
                karyawan_jabatan_id = '{$jabatan_id}',
                nama = '{$nama}',
                alamat = '{$alamat}',
                nohp = '{$nohp}',
                status = '{$status}',
                no_rek = '{$no_rek}',
                gaji_pokok = '{$gaji_pokok}',
                tunjangan = '{$tunjangan}',
                uang_makan = '{$uang_makan}',
                transport = '{$transport}'";
        $total = ($gaji_pokok+$tunjangan+$uang_makan+$transport);
        $gaji = $total + ($total*($pajak/100));
        if (mysqli_query($koneksi, $karyawan)) {
            mysqli_query($koneksi, "INSERT INTO gaji (gaji_user_id,pajak,gaji)
                VALUES ({$id_karyawan},'{$pajak}','{$gaji}')
                    ON DUPLICATE KEY UPDATE
                pajak = '{$pajak}',
                gaji = '{$gaji}'");
            echo "<script LANGUAGE='JavaScript'>
                window.alert('Data Karyawan Berhasil Tersimpan')
                window.location.href='dashboard.php';
            </script>";
        }else {
            echo "Error: " . $karyawan . "<br>" . mysqli_error($koneksi);
        }

    } else {
        echo "Error: " . $users . "<br>" . mysqli_error($koneksi);
    }
}


if (isset($_GET['id'])) {
    $query = "SELECT * FROM users LEFT 
    JOIN karyawan ON id_karyawan = id_users 
    LEFT JOIN jabatan ON karyawan_jabatan_id = jabatan_id 
    LEFT JOIN gaji ON gaji_user_id = id_users WHERE id_users = {$_GET['id']}";
    $data =mysqli_query($koneksi,$query);
    if ($data){
        $data = mysqli_fetch_object($data);
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= @$data->id_users ? 'Update' : 'Tambah'?> Karyawan</title>
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <?= menu()?>

    <section class="dashboard mt-2">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page"><?= @$data->id_users ? 'Update' : 'Tambah'?> Data Karyawan</li>
                    </ol>
                </nav>
                <a href="dashboard.php" class="btn btn-sm btn-outline-primary">< Kembali</a><br><br>
                <form action="" method="POST">
                    <input type="hidden" name="id_users" value="<?= @$data->id_users ?: null ?>" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username"
                                       placeholder="masukkan username anda" required value="<?= @$data->username ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="masukkan username password" <?= @$data->id_users ? null : 'required' ?>>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select class="form-control" name="level" id="level">
                                    <option value="">--Pilih Level--</option>
                                    <option value="1" <?= @$data->level==='1' ? 'selected' : null ?>>1</option>
                                    <option value="2" <?= @$data->level==='2' ? 'selected' : null ?>>2</option>
                                    <option value="3" <?= @$data->level==='3' ? 'selected' : null ?>>3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="number" class="form-control" id="nik" name="nik"
                                       placeholder="masukkan nik anda" required value="<?= @$data->nik ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama">NAMA</label>
                                <input type="text" class="form-control" id="nama" name="nama"
                                       placeholder="masukkan nama anda" required value="<?= @$data->nama ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="alamat">ALAMAT</label>
                                <textarea class="form-control" name="alamat" id="alamat" cols="5" rows="5"
                                          required><?= @$data->alamat ?: null ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nohp">NO HANDPHONE</label>
                                <input type="text" class="form-control" id="nohp" name="nohp"
                                       placeholder="masukkan no hp anda" required value="<?= @$data->nohp ?: null ?>">
                            </div>
                            <div class="form-group">
                                <label for="karyawan_jabatan_id">Jabatan</label>
                                <select id="karyawan_jabatan_id" name="karyawan_jabatan_id" class="form-control" required>
                                    <option value="">Pilih Jabatan</option>
                                    <?php
                                        $q = "SELECT * FROM jabatan";
                                        $select =mysqli_query($koneksi,$q);
                                        if ($select){
                                            while ($obj = mysqli_fetch_object($select)) {
                                                $selected = @$data->karyawan_jabatan_id == $obj->jabatan_id ?: "selected";
                                                echo '<option value="'.$obj->jabatan_id.'" '.$selected.' >'.$obj->jabatan_nama.'</option>';
                                            }
                                        }else{
                                            echo '<option>Harap Mengisi Data Jabatan</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="status">STATUS</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="">--Pilih Status--</option>
                                    <option value="magang" <?= @$data->status=='magang' ? 'selected' : null ?>>Magang</option>
                                    <option value="kontrak" <?= @$data->status=='kontrak' ? 'selected' : null ?>>Kontrak</option>
                                    <option value="tetap" <?= @$data->status=='tetap' ? 'selected' : null ?>>Tetap</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="no_rek">NO Rekening</label>
                                <input type="text" class="form-control" id="no_rek" name="no_rek"
                                       placeholder="masukkan no rekening anda" required value="<?= @$data->no_rek ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gaji_pokok">Gaji Pokok</label>
                                <input type="number" class="form-control" id="gaji_pokok" name="gaji_pokok"
                                       placeholder="masukkan Gaji Pokok anda" required value="<?= @$data->gaji_pokok ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tunjangan">Tunjangan</label>
                                <input type="number" class="form-control" id="tunjangan" name="tunjangan"
                                       placeholder="masukkan tunjangan anda" required value="<?= @$data->tunjangan ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="transport">Transport</label>
                                <input type="number" class="form-control" id="transport" name="transport"
                                       placeholder="masukkan biaya transportasi" required value="<?= @$data->transport ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="pajak">Pajak</label>
                                <input type="number" class="form-control" id="pajak" name="pajak"
                                       placeholder="masukkan pajak" required value="<?= @$data->pajak ?: null ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="uang_makan">Uang Makan</label>
                                <input type="number" class="form-control" id="uang_makan" name="uang_makan"
                                       placeholder="masukkan biaya uang_makan" required value="<?= @$data->uang_makan ?: null ?>">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary" name="submit">Simpan</button>
                </form>
            </div>
        </div>
    </section>
</div>

<script src="../static/js/bootstrap.min.js"></script>
<script src="../static/js/jquery.min.js"></script>
</body>
</html>